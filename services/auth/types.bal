const map<int> UserTypeStringToIntMap = {
    hod: 0,
    lecturer: 1,
    student: 2
};

const map<string> UserTypeIntToStringMap = {
    "0": "hod",
    "1": "lecturer",
    "2": "student"
};

public type BaseUser record {
    string username;
    string password;
    string userType;
};

public type RegisterUserRequest BaseUser;

public type BackendUser record {
    int id;
    string username;
    string userType;
    # JWT auth token
    string token;
};

public type LoginUserRequest record {
    string username;
    string password;
};

public type SQLUser record {
    int userId;
    string username;
    string password;
    int userType;
};

public type ValidateTokenRequest record {
    string token;
};

// ===================== Kafka Messages ========================

public type BaseAuthMessage record {
    string requestId;
};

public type AuthRegisterMessage record {
    *BaseAuthMessage;
    string username;
    string password;
    string userType;
};

public type ErrorResponse record {
    int statusCode;
    string message;
};

public type AuthRegisterResponseMessage record {
    string requestId;
    BackendUser|ErrorResponse data;
};

public type AuthLoginMessage record {
    *BaseAuthMessage;
    string username;
    string password;
};

public type AuthValidateTokenMessage record {
    *BaseAuthMessage;
    string token;
};
