import ballerina/jwt;
import ballerinax/java.jdbc;
import ballerina/crypto;
import ballerina/sql;
import ballerinax/kafka;
import ballerina/log;

// Kafka leader cluster
string bootstrapServer = "localhost:9093";

const string dbUser = "rootUser";
const string dbPassword = "rootPass";

function connectToDatabase() returns jdbc:Client|error {
    return new jdbc:Client("jdbc:h2:file:./target/db/users", dbUser, dbPassword);
}

// Initialize user database
function initDatabase() returns sql:Error?|error {
    jdbc:Client db = check connectToDatabase();
    _ = check db->execute(`
        CREATE TABLE IF NOT EXISTS Users(
            userId IDENTITY PRIMARY KEY,
            username VARCHAR(30),
            password BINARY(32),
            userType INT
        )
    `);
}

function findUser(string username) returns SQLUser|error {
    jdbc:Client db = check connectToDatabase();
    stream<SQLUser, error?> resultStream = db->query(`SELECT * FROM Users WHERE username=${username} LIMIT 1`);
    record {|SQLUser value;|}|error? result = resultStream.next();
    if result is record {|SQLUser value;|} {
        return result.value;
    } else {
        return error("User not found");
    }
}

function registerUser(string username, byte[] hashedPassword, int userType) returns int|error {
    jdbc:Client db = check connectToDatabase();
    sql:ExecutionResult result = check db->execute(`
        INSERT INTO Users (username, password, userType)
        VALUES (${username}, ${hashedPassword}, ${userType}) 
    `);
    var lastId = result.lastInsertId;
    if lastId is string {
        return int:fromString(lastId);
    } else if (lastId is int) {
        return lastId;
    } else {
        return error("Failed to get ID of newly created user");
    }
}

function getKafkaProducer() returns (kafka:Producer)|error {
    return new kafka:Producer(bootstrapServer, {clientId: "api"});
}

function processKafkaRecord(kafka:ConsumerRecord kafkaRecord) returns json|error {
    string messageContent = check string:fromBytes(kafkaRecord.value);
    return messageContent.fromJsonString();
}

function processRecords(kafka:Caller caller, kafka:ConsumerRecord[] records) returns json[]|error {
    json[] processedRecords = [];
    // The set of Kafka records received by the service are processed one by one.
    foreach int i in 0 ... records.length() - 1 {
        processedRecords[i] = check processKafkaRecord(records[i]);
    }

    // Commits offsets of the returned records by marking them as consumed.
    kafka:Error? commitResult = caller->commit();

    if commitResult is error {
        log:printError("Error occurred while committing the offsets for the consumer ", 'error = commitResult);
    }

    return processedRecords;
}

function generateJwtToken(string username) returns string|error {
    jwt:IssuerConfig issuerConfig = {
        username: username,
        issuer: "NUST",
        expTime: 86400, // 24 hours
        signatureConfig: {
            config: {
                keyFile: "./resources/private.key"
            }
        }
    };
    return jwt:issue(issuerConfig);
}

function validateJwtToken(string token) returns string|error {
    jwt:ValidatorConfig config = {
        issuer: "NUST",
        signatureConfig: {
            certFile: "./resources/public.crt"
        }
    };
    jwt:Payload|jwt:Error response = jwt:validate(token, config);
    if response is jwt:Payload {
        // If the token is valid, return the username the token corresponds to
        string? username = response?.sub;
        if (username is string) {
            return username;
        } else {
            return error("Failed to extract username from token");
        }
    } else {
        return response;
    }
}

function authRegister(AuthRegisterMessage payload) returns BackendUser|ErrorResponse|error {
    // Check if the user already exists
    SQLUser|error existingUser = findUser(payload.username);
    if existingUser is SQLUser {
        return <ErrorResponse>{statusCode: 409, message: "User already exists"};
    }

    // Hash the user's password using SHA-256
    byte[] hashedPassword = crypto:hashSha256(payload.password.toBytes());

    int? userTypeInt = UserTypeStringToIntMap[payload.userType];
    if !(userTypeInt is int) {
        return <ErrorResponse>{statusCode: 400, message: "Invalid user type. User must be one of: [hod, lecturer, student]"};
    } else {
        // Add the new user to the database.
        int newUserId = check registerUser(payload.username, hashedPassword, userTypeInt);
        string token = check generateJwtToken(payload.username);
        BackendUser newUser = {
            id: newUserId,
            username: payload.username,
            userType: payload.userType,
            token: token
        };
        return newUser;
    }
}

function authLogin(LoginUserRequest payload) returns BackendUser|ErrorResponse|error {
    // Find the user in the db
    SQLUser|error existingUser = findUser(payload.username);
    if existingUser is SQLUser {
        byte[] hashedPassword = crypto:hashSha256(payload.password.toBytes());
        // Compare the passwords using base16 since that's the default response from the DB.
        if (existingUser.password == hashedPassword.toBase16()) {
            // Password matches so generate a JWT token
            string token = check generateJwtToken(payload.username);
            string? userType = UserTypeIntToStringMap[existingUser.userType.toString()];
            if userType is string {
                BackendUser newUser = {
                    id: existingUser.userId,
                    username: payload.username,
                    userType: userType,
                    token: token
                };
                return newUser;
            } else {
                return <ErrorResponse>{statusCode: 400, message: "Failed to parse user type: " + existingUser.userType.toString()};
            }
        }
    }
    return <ErrorResponse>{statusCode: 401, message: "Invalid credentials"};
}

function authValidateToken(ValidateTokenRequest payload) returns BackendUser|ErrorResponse|error {
    string|error username = validateJwtToken(payload.token);
    if username is string {
        // If the token is valid, return its corresponding user
        SQLUser|error existingUser = findUser(username);
        if existingUser is SQLUser {
            string? userType = UserTypeIntToStringMap[existingUser.userType.toString()];
            if userType is string {
                BackendUser user = {
                    id: existingUser.userId,
                    token: payload.token,
                    userType: userType,
                    username: existingUser.username
                };
                return user;
            } else {
                return <ErrorResponse>{statusCode: 400, message: "Failed to parse user type: " + existingUser.userType.toString()};
            }
        }
    }
    return <ErrorResponse>{statusCode: 401, message: "Invalid credentials"};
}

function processTopic(kafka:Caller caller, kafka:ConsumerRecord[] records, string topic) returns error? {
    json[] processedRecords = check processRecords(caller, records);
    foreach var processedRecord in processedRecords {
        kafka:Producer kafkaProducer = check getKafkaProducer();
        BaseAuthMessage msg = check processedRecord.cloneWithType(BaseAuthMessage);
        if topic == "auth_register" {
            var response = check authRegister(check processedRecord.cloneWithType(AuthRegisterMessage));
            check kafkaProducer->send({
                topic: "auth_register_response",
                value: {
                    requestId: msg.requestId,
                    data: response
                }.toJsonString().toBytes()
            });
        } else if topic == "auth_login" {
            var response = check authLogin(check processedRecord.cloneWithType(AuthLoginMessage));
            check kafkaProducer->send({
                topic: "auth_login_response",
                value: {
                    requestId: msg.requestId,
                    data: response
                }.toJsonString().toBytes()
            });
        } else if topic == "auth_validate" {
            var response = check authValidateToken(check processedRecord.cloneWithType(AuthValidateTokenMessage));
            check kafkaProducer->send({
                topic: "auth_validate_response",
                value: {
                    requestId: msg.requestId,
                    data: response
                }.toJsonString().toBytes()
            });
        }
        check kafkaProducer->close();
    }
}

service kafka:Service on new kafka:Listener(bootstrapServer, {
    groupId: "auth_register",
    topics: ["auth_register"],
    pollingInterval: 1,
    autoCommit: false
}) {
    remote function onConsumerRecord(kafka:Caller caller, kafka:ConsumerRecord[] records) returns error? {
        string topic = "auth_register";
        error? ret = processTopic(caller, records, topic);
        if ret is error {
            log:printError("[topic:" + topic + "]: ", ret);
        }
    }
}

service kafka:Service on new kafka:Listener(bootstrapServer, {
    groupId: "auth_login",
    topics: ["auth_login"],
    pollingInterval: 1,
    autoCommit: false
}) {
    remote function onConsumerRecord(kafka:Caller caller, kafka:ConsumerRecord[] records) returns error? {
        string topic = "auth_login";
        error? ret = processTopic(caller, records, topic);
        if ret is error {
            log:printError("[topic:" + topic + "]: ", ret);
        }
    }
}

service kafka:Service on new kafka:Listener(bootstrapServer, {
    groupId: "auth_validate",
    topics: ["auth_validate"],
    pollingInterval: 1,
    autoCommit: false
}) {
    remote function onConsumerRecord(kafka:Caller caller, kafka:ConsumerRecord[] records) returns error? {
        string topic = "auth_validate";
        error? ret = processTopic(caller, records, topic);
        if ret is error {
            log:printError("[topic:" + topic + "]: ", ret);
        }
    }
}

public function main() returns error? {
    check initDatabase();
}
