import ballerina/http;
import ballerinax/kafka;
import ballerina/uuid;
import ballerina/log;

string bootstrapServer = "localhost:9093";

function getKafkaProducer() returns (kafka:Producer)|error {
    return new kafka:Producer(bootstrapServer, {clientId: "api"});
}

function processKafkaRecord(kafka:ConsumerRecord kafkaRecord) returns json|error {
    string messageContent = check string:fromBytes(kafkaRecord.value);
    return messageContent.fromJsonString();
}

kafka:Consumer apiResponseConsumer = check new (bootstrapServer, {
    groupId: "api_response",
    topics: ["api_response"],
    pollingInterval: 1,
    autoCommit: true,
    offsetReset: "latest"
});

kafka:Consumer authRegisterResponseConsumer = check new (bootstrapServer, {
    groupId: "auth_register_response",
    topics: ["auth_register_response"],
    pollingInterval: 1,
    autoCommit: true,
    offsetReset: "latest"
});

kafka:Consumer authLoginResponseConsumer = check new (bootstrapServer, {
    groupId: "auth_login_response",
    topics: ["auth_login_response"],
    pollingInterval: 1,
    autoCommit: true,
    offsetReset: "latest"
});

kafka:Consumer authValidateResponseConsumer = check new (bootstrapServer, {
    groupId: "auth_validate_response",
    topics: ["auth_validate_response"],
    pollingInterval: 1,
    autoCommit: true,
    offsetReset: "latest"
});

function readRequestResponse(kafka:Consumer consumer, string requestId) returns json|http:BadRequest|error {
    kafka:ConsumerRecord[] records = check consumer->poll(5);
    log:printInfo("polled consumer and got " + records.length().toString() + " records");
    if records.length() > 0 {
        foreach var r in records {
            json processed = check processKafkaRecord(r);
            var msg = check processed.cloneWithType(RequestResponseMessage);
            // Only return responses for this specific request
            if msg.requestId == requestId {
                return processed;
            }
        }
    }
    return <http:BadRequest>{body: "Error getting response"};
}

function apiResponseToHttp(ApiResponseMessage data) returns http:BadRequest|http:Conflict|http:BadRequest|ApiResponseMessage {
    string? errorMsg = data.'error;
    if data is ErrorResponse {
        if (data.statusCode == 400) {
            return <http:BadRequest>{body: data.message};
        } else if (data.statusCode == 409) {
            return <http:Conflict>{body: data.message};
        } else {
            return <http:BadRequest>{body: "Unknown error"};
        }
    } else if (errorMsg is string) {
        return <http:BadRequest>{body: errorMsg};
    } else {
        return data;
    }
}

function readResponseErrors(string requestId) returns ApiResponseMessage|http:BadRequest|http:Conflict|error {
    var processed = readRequestResponse(apiResponseConsumer, requestId);
    if processed is json {
        ApiResponseMessage msg = check processed.cloneWithType(ApiResponseMessage);
        return apiResponseToHttp(msg);
    } else {
        return processed;
    }
}

function verifyToken(string token) returns BackendUser|http:BadRequest|http:Unauthorized|error {
    kafka:Producer kafkaProducer = check getKafkaProducer();
    string requestId = uuid:createType4AsString();
    AuthValidateTokenMessage message = {
        requestId: requestId,
        token: token
    };
    check kafkaProducer->send({
        topic: "auth_validate",
        value: message.toJsonString().toBytes()
    });
    check kafkaProducer->close();

    var processed = readRequestResponse(authValidateResponseConsumer, requestId);
    if processed is json {
        AuthValidateTokenResponseMessage response = check processed.cloneWithType(AuthValidateTokenResponseMessage);
        BackendUser|ErrorResponse data = response.data;
        if data is BackendUser {
            return data;
        } else if (data.statusCode == 400) {
            return <http:BadRequest>{body: data.message};
        } else if (data.statusCode == 401) {
            return <http:Unauthorized>{body: data.message};
        } else {
            return <http:BadRequest>{body: "Unknown error"};
        }
    } else {
        return processed;
    }
}

function sendKafkaMessage(string topic, record {string requestId;} message) returns ApiResponseMessage|http:BadRequest|http:Conflict|error {
    kafka:Producer kafkaProducer = check getKafkaProducer();
    check kafkaProducer->send({
        topic: topic,
        value: message.toJsonString().toBytes()
    });
    check kafkaProducer->close();
    return readResponseErrors(message.requestId);
}

// TODO: Consider using `@http:Header string Authorization``

service /v1 on new http:Listener(9090) {
    resource function post auth/register(@http:Payload {} RegisterUserRequest payload) returns BackendUser|http:BadRequest|http:Conflict|error {
        kafka:Producer kafkaProducer = check getKafkaProducer();
        string requestId = uuid:createType4AsString();
        AuthRegisterMessage message = {
            requestId: requestId,
            username: payload.username,
            password: payload.password,
            userType: payload.userType
        };
        check kafkaProducer->send({
            topic: "auth_register",
            value: message.toJsonString().toBytes()
        });
        check kafkaProducer->close();

        var processed = readRequestResponse(authRegisterResponseConsumer, requestId);
        if processed is json {
            AuthLoginResponseMessage response = check processed.cloneWithType(AuthLoginResponseMessage);
            BackendUser|ErrorResponse data = response.data;
            if data is BackendUser {
                return data;
            } else if (data.statusCode == 400) {
                return <http:BadRequest>{body: data.message};
            } else if (data.statusCode == 409) {
                return <http:Conflict>{body: data.message};
            } else {
                return <http:BadRequest>{body: "Unknown error"};
            }
        } else {
            return processed;
        }
    }

    resource function post auth/login(@http:Payload {} LoginUserRequest payload) returns BackendUser|http:BadRequest|http:Unauthorized|error {
        kafka:Producer kafkaProducer = check getKafkaProducer();
        string requestId = uuid:createType4AsString();
        AuthLoginMessage message = {
            requestId: requestId,
            username: payload.username,
            password: payload.password
        };
        check kafkaProducer->send({
            topic: "auth_login",
            value: message.toJsonString().toBytes()
        });
        check kafkaProducer->close();

        var processed = readRequestResponse(authLoginResponseConsumer, requestId);
        if processed is json {
            AuthLoginResponseMessage response = check processed.cloneWithType(AuthLoginResponseMessage);
            BackendUser|ErrorResponse data = response.data;
            if data is BackendUser {
                return data;
            } else if (data.statusCode == 400) {
                return <http:BadRequest>{body: data.message};
            } else if (data.statusCode == 401) {
                return <http:Unauthorized>{body: data.message};
            } else {
                return <http:BadRequest>{body: "Unknown error"};
            }
        } else {
            return processed;
        }
    }

    resource function post course/view(string token, string[]? courseIds) returns CourseOutline[]|ApiResponseMessage|http:Ok|http:BadRequest|http:Conflict|http:Unauthorized|error {
        var user = verifyToken(token);
        if !(user is BackendUser) {
            return user;
        } else {
            CourseViewMessage message = {
                requestId: uuid:createType4AsString(),
                user: user,
                courseIds: courseIds
            };
            var rawResponse = sendKafkaMessage("course_view", message);
            if rawResponse is ApiResponseMessage {
                json processed = check rawResponse.message.fromJsonString();
                CourseViewResponseMessage response = check processed.cloneWithType(CourseViewResponseMessage);
                return response.courses;
            } else {
                return rawResponse;
            }
        }
    }

    resource function post course/acknowledge(string token, string courseId) returns ApiResponseMessage|http:Ok|http:BadRequest|http:Conflict|http:Unauthorized|error {
        var user = verifyToken(token);
        if !(user is BackendUser) {
            return user;
        } else if (!(user.userType == "student")) {
            return <http:Unauthorized>{body: "User must be a student"};
        } else {
            CourseAcknowledgeMessage message = {
                requestId: uuid:createType4AsString(),
                user: user,
                courseId: courseId
            };
            return sendKafkaMessage("course_acknowledge", message);
        }
    }

    resource function post course/update(string token, string courseId, @http:Payload {} CourseOutline payload) returns ApiResponseMessage|http:Ok|http:BadRequest|http:Conflict|http:Unauthorized|error {
        var user = verifyToken(token);
        if !(user is BackendUser) {
            return user;
        } else if (!(user.userType == "lecturer" || user.userType == "hod")) {
            return <http:Unauthorized>{body: "User must be a lecturer or a head of department"};
        } else {
            CourseUpdateMessage message = {
                requestId: uuid:createType4AsString(),
                user: user,
                courseId: courseId,
                data: payload
            };
            return sendKafkaMessage("course_update", message);
        }
    }

    resource function post course/generate(string token, string courseId) returns ApiResponseMessage|http:BadRequest|http:Conflict|http:Unauthorized|error {
        var user = verifyToken(token);
        if !(user is BackendUser) {
            return user;
        } else if (!(user.userType == "lecturer")) {
            return <http:Unauthorized>{body: "User must be a lecturer"};
        } else {
            kafka:Producer kafkaProducer = check getKafkaProducer();
            CourseGenerateMessage message = {
                requestId: uuid:createType4AsString(),
                user: user,
                courseId: courseId
            };
            return sendKafkaMessage("course_generate", message);
        }
    }

    resource function post course/approve(string token, string courseId) returns ApiResponseMessage|http:Ok|http:BadRequest|http:Conflict|http:Unauthorized|error {
        var user = verifyToken(token);
        if !(user is BackendUser) {
            return user;
        } else if (!(user.userType == "lecturer" || user.userType == "hod")) {
            return <http:Unauthorized>{body: "User must be a lecturer or a head of department"};
        } else {
            CourseApproveMessage message = {
                requestId: uuid:createType4AsString(),
                user: user,
                courseId: courseId
            };
            return sendKafkaMessage("course_approve", message);
        }
    }

    resource function post lecturer/update(@http:Payload {} LecturerUpdatePayload payload) returns ApiResponseMessage|http:Ok|http:BadRequest|http:Conflict|http:Unauthorized|error {
        var user = verifyToken(payload.token);
        if !(user is BackendUser) {
            return user;
        } else if (user.userType != "hod") {
            return <http:Unauthorized>{body: "User must be a head of department"};
        } else {
            LecturerUpdateMessage message = {
                requestId: uuid:createType4AsString(),
                user: user,
                courseIds: payload.courseIds,
                lecturer: payload.lecturer
            };
            return sendKafkaMessage("lecturer_update", message);
        }
    }

    resource function post student/update(@http:Payload {} StudentUpdatePayload payload) returns ApiResponseMessage|http:Ok|http:BadRequest|http:Conflict|http:Unauthorized|error {
        var user = verifyToken(payload.token);
        if !(user is BackendUser) {
            return user;
        } else if (user.userType != "hod") {
            return <http:Unauthorized>{body: "User must be a head of department"};
        } else {
            StudentUpdateMessage message = {
                requestId: uuid:createType4AsString(),
                user: user,
                courseIds: payload.courseIds,
                studentId: payload.studentId
            };
            return sendKafkaMessage("student_update", message);
        }
    }
}

public function main() returns error? {
    _ = check apiResponseConsumer->poll(1);
    _ = check authRegisterResponseConsumer->poll(1);
    _ = check authLoginResponseConsumer->poll(1);
    _ = check authValidateResponseConsumer->poll(1);
}
