public type RegisterUserRequest record {
    string username;
    string password;
    string userType;
};

public type LoginUserRequest record {
    string username;
    string password;
};

public type BaseUser record {
    string username;
    string password;
    string userType;
};

public type BackendUser record {
    int id;
    string username;
    string userType;
    # JWT auth token
    string token;
};

public type Lecturer record {
    int id;
    string name;
    string email?;
    string officePhone?;
    string officeLocation?;
    string officeHours?;
    boolean isHeadOfDepartment?;
};

public type CourseTopic record {
    int id;
    string name;
    string description?;
    CourseTopic[] subTopics?;
};

public type CourseAssessment record {
    int id;
    string name;
    # Percentage that this assessment will contribute to final marks
    int weight;
    string description?;
};

public type CoursePrescribedResource record {
    string name;
    string? description;
};

public type CourseSchedule record {
    int topicId;
    string[] dates?;
};

public type CourseAssessmentSchedule record {
    int assessmentId;
    # Date assessment will be given to students in YYYY-MM-DD format
    string releaseDate?;
    # Date in YYYY-MM-DD format
    string dueDate;
};

public type CourseOutline record {
    string courseId;
    string name;
    string description;
    # Any extra information about the course such as policies and statements about integrity.
    string extraInfo;
    string faculty;
    string department;
    string programme;
    int contactHours;
    Lecturer[] lecturers;
    # The ID of the lecturer who is the head of this course's department
    int hodLecturerId;
    CoursePrescribedResource[] prescribedResources;
    string[] learningOutcomes;
    CourseTopic[] topics;
    # The dates each topic will be taught
    CourseSchedule[] schedule;
    CourseAssessment[] assessments;
    # The dates each assessment will be given and due
    CourseAssessmentSchedule[] assessmentSchedule;
};

public type CourseOutlineWithFlags record {
    *CourseOutline;
    boolean isGenerated;
    boolean isApproved;
    int[] approvedBy;
};

public type LecturerUpdatePayload record {
    string token;
    string[] courseIds;
    Lecturer lecturer;
};

public type StudentUpdatePayload record {
    string token;
    string[] courseIds;
    int studentId;
};

// ===================== Kafka Messages ========================

public type BaseAuthMessage record {
    string requestId;
};

public type AuthRegisterMessage record {
    *BaseAuthMessage;
    string username;
    string password;
    string userType;
};

public type ErrorResponse record {
    int statusCode;
    string message;
};

public type AuthRegisterResponseMessage record {
    string requestId;
    BackendUser|ErrorResponse data;
};

public type AuthLoginMessage record {
    *BaseAuthMessage;
    string username;
    string password;
};

public type AuthLoginResponseMessage AuthRegisterResponseMessage;

public type AuthValidateTokenMessage record {
    *BaseAuthMessage;
    string token;
};

public type AuthValidateTokenResponseMessage AuthRegisterResponseMessage;

public type BaseMessage record {
    string requestId;

    BackendUser user;
};

public type BaseCourseMessage record {
    *BaseMessage;
    string courseId;
};

public type CourseViewMessage record {
    *BaseMessage;
    string[]? courseIds;
};

public type CourseUpdateMessage record {
    *BaseCourseMessage;
    CourseOutline data;
};

public type CourseAcknowledgeMessage record {
    *BaseCourseMessage;
};

public type CourseGenerateMessage record {
    *BaseCourseMessage;
};

public type CourseApproveMessage record {
    *BaseCourseMessage;
};

public type LecturerUpdateMessage record {
    *BaseMessage;
    Lecturer lecturer;
    string[] courseIds;
};

public type StudentUpdateMessage record {
    *BaseMessage;
    int studentId;
    string[] courseIds;
};

public type RequestResponseMessage record {
    string requestId;
};

public type ApiResponseMessage record {
    *RequestResponseMessage;
    string? 'error;
    string message;
};

public type CourseViewResponseMessage record {
    CourseOutlineWithFlags[] courses;
};
