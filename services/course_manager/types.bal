public type BackendUser record {
    int id;
    string username;
    string userType;
    # JWT auth token
    string token;
};

public type Lecturer record {
    int id;
    string name;
    string email?;
    string officePhone?;
    string officeLocation?;
    string officeHours?;
    boolean isHeadOfDepartment?;
};

public type CourseTopic record {
    int id;
    string name;
    string description?;
    CourseTopic[] subTopics?;
};

public type CourseAssessment record {
    int id;
    string name;
    # Percentage that this assessment will contribute to final marks
    int weight;
    string description?;
};

public type CoursePrescribedResource record {
    string name;
    string? description;
};

public type StringArray string[];

public type IntArray int[];

public type CourseTopicArray CourseTopic[];

public type CourseAssessmentArray CourseAssessment[];

public type CourseScheduleArray CourseSchedule[];

public type CoursePrescribedResourceArray CoursePrescribedResource[];

public type CourseAssessmentScheduleArray CourseAssessmentSchedule[];

public type CourseSchedule record {
    int topicId;
    string[] dates?;
};

public type CourseAssessmentSchedule record {
    int assessmentId;
    # Date assessment will be given to students in YYYY-MM-DD format
    string releaseDate?;
    # Date in YYYY-MM-DD format
    string dueDate;
};

public type CourseOutline record {
    string courseId;
    string name;
    string description;
    # Any extra information about the course such as policies and statements about integrity.
    string extraInfo;
    string faculty;
    string department;
    string programme;
    int contactHours;
    Lecturer[] lecturers;
    # The ID of the lecturer who is the head of this course's department
    int hodLecturerId;
    CoursePrescribedResource[] prescribedResources;
    string[] learningOutcomes;
    CourseTopic[] topics;
    # The dates each topic will be taught
    CourseSchedule[] schedule;
    CourseAssessment[] assessments;
    # The dates each assessment will be given and due
    CourseAssessmentSchedule[] assessmentSchedule;
};

public type CourseOutlineWithFlags record {
    *CourseOutline;
    boolean isGenerated;
    boolean isApproved;
    int[] approvedBy;
};

// ===================== Kafka Messages ========================

public type BaseMessage record {
    string requestId;

    BackendUser user;
};

public type BaseCourseMessage record {
    *BaseMessage;
    string courseId;
};

public type CourseViewMessage record {
    *BaseMessage;
    string[]? courseIds;
};

public type CourseUpdateMessage record {
    *BaseCourseMessage;
    CourseOutline data;
};

public type CourseAcknowledgeMessage record {
    *BaseCourseMessage;
};

public type CourseGenerateMessage record {
    *BaseCourseMessage;
};

public type CourseApproveMessage record {
    *BaseCourseMessage;
};

public type LecturerUpdateMessage record {
    *BaseMessage;
    Lecturer lecturer;
    string[] courseIds;
};

public type StudentUpdateMessage record {
    *BaseMessage;
    int studentId;
    string[] courseIds;
};

public type ApiResponseMessage record {
    string requestId;
    string? 'error;
    string message;
};

public type CourseViewResponseMessage record {
    CourseOutlineWithFlags[] courses;
};

// ===================== SQL ========================

public type SQLCourseOutline record {
    string courseId;
    string name;
    string? description;
    string? extraInfo;
    string faculty;
    string department;
    string programme;
    int? contactHours;
    int hodLecturerId;
    string prescribedResources;
    string learningOutcomes;
    string topics;
    string schedule;
    string assessments;
    string assessmentSchedule;
    boolean isGenerated;
    boolean isApproved;
    string approvedBy;
};

public type SQLLecturer Lecturer;
