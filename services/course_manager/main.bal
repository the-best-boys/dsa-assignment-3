import ballerinax/java.jdbc;
import ballerina/sql;
import ballerinax/kafka;
import ballerina/log;

// Kafka leader cluster
string bootstrapServer = "localhost:9093";

string dbUser = "rootUser";
string dbPassword = "rootPass";

function getKafkaProducer() returns (kafka:Producer)|error {
    kafka:ProducerConfiguration producerConfiguration = {clientId: "course_manager"};
    return new kafka:Producer(bootstrapServer, producerConfiguration);
}

function connectToDatabase() returns jdbc:Client|error {
    return new jdbc:Client("jdbc:h2:file:./target/db/course_manager", dbUser, dbPassword);
}

// Initialize user database
function initDatabase() returns sql:Error?|error {
    jdbc:Client db = check connectToDatabase();
    _ = check db->execute(`
        CREATE TABLE IF NOT EXISTS Lecturers(
            lecturerId IDENTITY PRIMARY KEY
            ,name                    VARCHAR(100) NOT NULL
            ,email                   VARCHAR(50) NOT NULL
            ,officePhone             VARCHAR(20) NOT NULL
            ,officeLocation          VARCHAR(100) NOT NULL
            ,officeHours             VARCHAR(50) NOT NULL
            ,isHeadOfDepartment      BIT NOT NULL
        );

        CREATE TABLE IF NOT EXISTS Courses(
            courseId                          VARCHAR(10) NOT NULL PRIMARY KEY
            ,name                              VARCHAR(40) NOT NULL
            ,description                       VARCHAR(200)
            ,extraInfo                         VARCHAR(200)
            ,faculty                           VARCHAR(50) NOT NULL
            ,department                        VARCHAR(50) NOT NULL
            ,programme                         VARCHAR(50) NOT NULL
            ,contactHours                      INTEGER
            ,hodLecturerId                     INT  NOT NULL
            ,prescribedResources          NVARCHAR(MAX) NOT NULL
            ,learningOutcomes                 NVARCHAR(MAX) NOT NULL
            ,topics                 NVARCHAR(MAX) NOT NULL
            ,schedule                 NVARCHAR(MAX) NOT NULL
            ,assessments                 NVARCHAR(MAX) NOT NULL
            ,assessmentSchedule                 NVARCHAR(MAX) NOT NULL
            ,isGenerated                       BIT NOT NULL DEFAULT 0
            ,isApproved                        BIT NOT NULL DEFAULT 0
            ,approvedBy                       NVARCHAR(MAX) NOT NULL DEFAULT 0
            ,FOREIGN KEY (hodLecturerId) REFERENCES Lecturers(lecturerId)
        );

        CREATE TABLE IF NOT EXISTS CourseLecturers(
            id IDENTITY PRIMARY KEY,
            courseId VARCHAR(10) NOT NULL,
            lecturerId INT NOT NULL,
            FOREIGN KEY (courseId) REFERENCES Courses(courseId),
            FOREIGN KEY (lecturerId) REFERENCES Lecturers(lecturerId),
        );

        CREATE TABLE IF NOT EXISTS Students(
            userId INT PRIMARY KEY
        );

        CREATE TABLE IF NOT EXISTS StudentCourses(
            id IDENTITY PRIMARY KEY,
            userId INT NOT NULL,
            courseId VARCHAR(10) NOT NULL,
            isAcknowledged BIT NOT NULL DEFAULT 0,
            FOREIGN KEY (courseId) REFERENCES Courses(courseId),
            FOREIGN KEY (userId) REFERENCES Students(userId),
        );
    `);
}

function processKafkaRecord(kafka:ConsumerRecord kafkaRecord) returns json|error {
    string messageContent = check string:fromBytes(kafkaRecord.value);
    return messageContent.fromJsonString();
}

function processRecords(kafka:Caller caller, kafka:ConsumerRecord[] records) returns json[]|error {
    json[] processedRecords = [];
    // The set of Kafka records received by the service are processed one by one.
    foreach int i in 0 ... records.length() - 1 {
        processedRecords[i] = check processKafkaRecord(records[i]);
    }

    // Commits offsets of the returned records by marking them as consumed.
    kafka:Error? commitResult = caller->commit();

    if commitResult is error {
        log:printError("Error occurred while committing the offsets for the consumer ", 'error = commitResult);
    }

    return processedRecords;
}

// ======================== Main business logic ==================================

function studentIsTakingCourse(jdbc:Client db, int userId, string courseId) returns boolean {
    stream<record {}, error?> resultStream = db->query(`SELECT * FROM StudentCourses WHERE userId=${userId} AND courseId=${courseId}`);
    record {|record {} value;|}|error? result = resultStream.next();
    if result is record {|record {int TOTAL;} value;|} {
        return result.value["TOTAL"] > 0;
    }
    return false;
}

function courseOutlineFromSqlCourse(SQLCourseOutline course, Lecturer[] lecturers) returns CourseOutlineWithFlags {
    string? description = course.description;
    string? extraInfo = course.extraInfo;
    int? contactHours = course.contactHours;
    CoursePrescribedResourceArray|error prescribedResources = course.prescribedResources.fromJsonStringWithType(CoursePrescribedResourceArray);
    StringArray|error learningOutcomes = course.learningOutcomes.fromJsonStringWithType(StringArray);
    CourseTopicArray|error topics = course.topics.fromJsonStringWithType(CourseTopicArray);
    CourseScheduleArray|error schedule = course.schedule.fromJsonStringWithType(CourseScheduleArray);
    CourseAssessmentArray|error assessments = course.assessments.fromJsonStringWithType(CourseAssessmentArray);
    CourseAssessmentScheduleArray|error assessmentSchedule = course.assessmentSchedule.fromJsonStringWithType(CourseAssessmentScheduleArray);
    IntArray|error approvedBy = course.approvedBy.fromJsonStringWithType(IntArray);

    CourseOutlineWithFlags processedCourse = {
        courseId: course.courseId,
        name: course.name,
        description: description is string ? description : "",
        extraInfo: extraInfo is string ? extraInfo : "",
        faculty: course.faculty,
        department: course.department,
        programme: course.programme,
        contactHours: contactHours is int ? contactHours : 0,
        hodLecturerId: course.hodLecturerId,
        prescribedResources: prescribedResources is CoursePrescribedResourceArray ? prescribedResources : [],
        learningOutcomes: learningOutcomes is StringArray ? learningOutcomes : [],
        topics: topics is CourseTopicArray ? topics : [],
        schedule: schedule is CourseScheduleArray ? schedule : [],
        assessments: assessments is CourseAssessmentArray ? assessments : [],
        assessmentSchedule: assessmentSchedule is CourseAssessmentScheduleArray ? assessmentSchedule : [],
        lecturers: lecturers,
        isGenerated: course.isGenerated,
        isApproved: course.isApproved,
        approvedBy: approvedBy is IntArray ? approvedBy : []
    };
    return processedCourse;
}

function viewCourse(CourseViewMessage request) returns string|error {
    jdbc:Client db = check connectToDatabase();
    SQLCourseOutline[] courseOutlines = [];
    // If user is student, return only their courses
    if (request.user.userType == "student") {
        string[]? courseIds = request.courseIds;
        if courseIds is string[] {
            foreach var courseId in courseIds {
                boolean studentHasCourse = studentIsTakingCourse(db, request.user.id, courseId);
                stream<SQLCourseOutline, error?> resultStream = db->query(`
                    SELECT Courses.* FROM Courses 
                    INNER JOIN StudentCourses 
                    ON StudentCourses.courseId=Courses.courseId
                    WHERE StudentCourses.userId = ${request.user.id}
                `);
                check resultStream.forEach(function(SQLCourseOutline result) {
                    courseOutlines.push(result);
                });
            }
        } else {
            return error("No course IDs provided");
        }
    } 
    // If user is HoD, return all in their department
    else if (request.user.userType == "hod") {
        string[]? courseIds = request.courseIds;
        if courseIds is string[] {
            foreach var courseId in courseIds {
                boolean studentHasCourse = studentIsTakingCourse(db, request.user.id, courseId);
                stream<SQLCourseOutline, error?> resultStream = db->query(`
                    SELECT * FROM Courses 
                    WHERE hodLecturerId=${request.user.id} AND courseId=${courseId}
                `);
                check resultStream.forEach(function(SQLCourseOutline result) {
                    courseOutlines.push(result);
                });
            }
        } else {
            stream<SQLCourseOutline, error?> resultStream = db->query(`
                SELECT * FROM Courses 
                WHERE hodLecturerId=${request.user.id}
            `);
            check resultStream.forEach(function(SQLCourseOutline result) {
                courseOutlines.push(result);
            });
        }
    } 
    // If user is lecturer, return all their courses
    else if (request.user.userType == "lecturer") {
        string[]? courseIds = request.courseIds;
        if courseIds is string[] {
            foreach var courseId in courseIds {
                boolean studentHasCourse = studentIsTakingCourse(db, request.user.id, courseId);
                stream<SQLCourseOutline, error?> resultStream = db->query(`
                    SELECT Courses.* FROM Courses 
                    INNER JOIN CourseLecturers 
                    ON CourseLecturers.courseId=Courses.courseId
                    WHERE CourseLecturers.lecturerId=${request.user.id}
                `);
                check resultStream.forEach(function(SQLCourseOutline result) {
                    courseOutlines.push(result);
                });
            }
        } else {
            return error("No course IDs provided");
        }
    } else {
        return error("Unknown user type");
    }

    CourseOutlineWithFlags[] courseOutlinesProcessed = [];
    foreach SQLCourseOutline course in courseOutlines {
        Lecturer[] lecturers = [];
        stream<SQLLecturer, error?> resultStream = db->query(`
            SELECT

            Lecturers.lecturerId as id,
            Lecturers.name,
            Lecturers.email,
            Lecturers.officePhone,
            Lecturers.officeLocation,
            Lecturers.officeHours,
            Lecturers.isHeadOfDepartment

            FROM Courses
            INNER JOIN CourseLecturers
            ON CourseLecturers.courseId=Courses.courseId
            INNER JOIN Lecturers
            ON Lecturers.lecturerId=CourseLecturers.lecturerId
            WHERE Lecturers.lecturerId=${request.user.id} AND Courses.courseId=${course.courseId}
        `);
        check resultStream.forEach(function(SQLLecturer lecturer) {
            lecturers.push(lecturer);
        });
        courseOutlinesProcessed.push(courseOutlineFromSqlCourse(course, lecturers));
    }

    CourseViewResponseMessage message = {
        courses: courseOutlinesProcessed
    };
    return message.toJsonString();
}

function updateLecturer(jdbc:Client db, Lecturer lecturer) returns error? {
    _ = check db->execute(`
        MERGE INTO Lecturers (
            lecturerId,
            name,
            email,
            officePhone,
            officeLocation,
            officeHours,
            isHeadOfDepartment
        )
        KEY (lecturerId)
        VALUES (
            ${lecturer.id},
            ${lecturer.name},
            ${lecturer?.email},
            ${lecturer?.officePhone},
            ${lecturer?.officeLocation},
            ${lecturer?.officeHours},
            ${lecturer?.isHeadOfDepartment == true ? 1 : 0}
        )
    `);
    log:printInfo("Updated details of lecturer " + lecturer.id.toString());
}

function lecturerIsAssignedToCourse(jdbc:Client db, int lecturerId, string courseId) returns boolean {
    stream<record {}, error?> resultStream = db->query(`SELECT * FROM CourseLecturers WHERE lecturerId=${lecturerId} AND courseId=${courseId}`);
    record {|record {} value;|}|error? result = resultStream.next();
    if result is record {|record {int TOTAL;} value;|} {
        return result.value["TOTAL"] > 0;
    }
    return false;
}

function assignLecturerToCourse(jdbc:Client db, int lecturerId, string courseId) returns error? {
    if (!lecturerIsAssignedToCourse(db, lecturerId, courseId)) {
        _ = check db->execute(`
            INSERT INTO CourseLecturers (lecturerId, courseId)
            VALUES (${lecturerId}, ${courseId}) 
        `);
        log:printInfo("Assigned lecturer " + lecturerId.toString() + " to course " + courseId.toString());
    } else {
        log:printInfo("Lecturer " + lecturerId.toString() + " is already assigned to course " + courseId.toString());
    }
}

function updateCourseLecturers(jdbc:Client db, CourseUpdateMessage request) returns error? {
    foreach var lecturer in request.data.lecturers {
        check updateLecturer(db, lecturer);
        check assignLecturerToCourse(db, lecturer.id, request.courseId);
    }
}

function updateCourse(CourseUpdateMessage request) returns error|string {
    // Only allow updating ones the lecturer has been assigned to
    if (request.user.userType == "lecturer" || request.user.userType == "hod") {
        jdbc:Client db = check connectToDatabase();
        boolean isAssigned = lecturerIsAssignedToCourse(db, request.user.id, request.courseId);
        if (request.user.userType == "hod" || isAssigned) {
            check updateCourseLecturers(db, request);
            var data = request.data;
            _ = check db->execute(`
                MERGE INTO Courses (
                    courseId,
                    name,
                    description,
                    extraInfo,
                    faculty,
                    department,
                    programme,
                    contactHours,
                    hodLecturerId,
                    prescribedResources,
                    learningOutcomes,
                    topics,
                    schedule,
                    assessments,
                    assessmentSchedule,
                    isGenerated,
                    isApproved,
                    approvedBy
                )
                KEY (courseId)
                VALUES (
                    ${request.courseId},
                    ${data.name},
                    ${data.description},
                    ${data.extraInfo},
                    ${data.faculty},
                    ${data.department},
                    ${data.programme},
                    ${data.contactHours},
                    ${data.hodLecturerId},
                    ${data.prescribedResources.toJsonString()},
                    ${data.learningOutcomes.toJsonString()},
                    ${data.topics.toJsonString()},
                    ${data.schedule.toJsonString()},
                    ${data.assessments.toJsonString()},
                    ${data.assessmentSchedule.toJsonString()},
                    ${0},
                    ${0},
                    ${[].toJsonString()}
                )
            `);
            return "Course updated";
        } else {
            return error("User is not assigned to the specified course and has no permission to edit it");
        }
    } else {
        return error("User must be a lecturer or head of department");
    }
}

function acknowledgeCourse(CourseAcknowledgeMessage request) returns string|error {
    if (request.user.userType == "student") {
        jdbc:Client db = check connectToDatabase();
        _ = check db->execute(`
            UPDATE StudentCourses
            SET isAcknowledged = 1
            WHERE courseId=${request.courseId} AND userId=${request.user.id}
        `);
        return "Acknowledged course " + request.courseId.toString() + " as user with ID " + request.user.id.toString();
    } else {
        return error("User must be a student");
    }
}

function generateCourse(CourseGenerateMessage request) returns string|error {
    if (request.user.userType == "lecturer") {
        jdbc:Client db = check connectToDatabase();
        _ = check db->execute(`
            UPDATE Courses
            SET isGenerated = 1
            WHERE courseId = ${request.courseId}
        `);
        return "Lecturer with ID " + request.user.id.toString() + " generated course " + request.courseId.toString();
    } else {
        return error("User must be a lecturer");
    }
}

function approveCourse(CourseApproveMessage request) returns string|error {
    if (request.user.userType == "lecturer" || request.user.userType == "hod") {
        jdbc:Client db = check connectToDatabase();

        // Check which lecturers already approved the course
        stream<record {string approvedBy;}, error?> resultStream = db->query(`
            SELECT approvedBy FROM Courses 
            WHERE courseId=${request.courseId}
        `);
        record {|record {string approvedBy;} value;|}? result = check resultStream.next();
        if result is record {|record {string approvedBy;} value;|} {
            var course = result.value;
            int[] approvedBy = check course.approvedBy.fromJsonStringWithType(IntArray);

            // Mark the course as being approved by this lecturer specifically
            approvedBy.push(request.user.id);
            _ = check db->execute(`
                UPDATE Courses
                SET approvedBy = ${approvedBy.toJsonString()}
                WHERE courseId = ${request.courseId}
            `);

            // Mark the course as approved
            _ = check db->execute(`
                UPDATE Courses
                SET isApproved = 1
                WHERE courseId = ${request.courseId}
            `);
            if (request.user.userType == "lecturer") {
                return "Lecturer with ID " + request.user.id.toString() + " approved course " + request.courseId.toString();
            } else {
                return "Head of department with ID " + request.user.id.toString() + " approved course " + request.courseId.toString();
            }
        } else {
            return error("User is not assigned to the course or the course does not exist");
        }
    } else {
        return error("User must be a lecturer or a head of department");
    }
}

function lecturerUpdate(LecturerUpdateMessage request) returns error|string {
    if (request.user.userType == "hod") {
        jdbc:Client db = check connectToDatabase();
        check updateLecturer(db, request.lecturer);

        foreach var courseId in request.courseIds {
            // Remove previous course definition so we can replace it
            sql:ExecutionResult|sql:Error ret = check db->execute(`
                DELETE FROM CourseLecturers
                WHERE lecturerId = ${request.lecturer.id}
            `);
            check assignLecturerToCourse(db, request.lecturer.id, courseId);
        }
        return "Lecturer updated";
    } else {
        return error("User must be a head of department");
    }
}

function studentUpdate(StudentUpdateMessage request) returns error|string {
    if (request.user.userType == "hod") {
        jdbc:Client db = check connectToDatabase();
        _ = check db->execute(`
            MERGE INTO Students (userId)
            KEY(userId)
            VALUES (${request.studentId}) 
        `);

        foreach var courseId in request.courseIds {
            // Remove previous course definition so we can replace it
            sql:ExecutionResult|sql:Error ret = check db->execute(`
                DELETE FROM StudentCourses
                WHERE userId = ${request.studentId}
            `);
            _ = check db->execute(`
                INSERT INTO StudentCourses (userId, courseId)
                VALUES (${request.studentId}, ${courseId}) 
            `);
        }
        return "Student updated successfully.";
    } else {
        return error("User must be a head of department");
    }
}

// ======================== Kafka Message Handling ==================================

function processTopic(kafka:Caller caller, kafka:ConsumerRecord[] records, string topic) returns error? {
    json[] processedRecords = check processRecords(caller, records);
    log:printInfo("[topic:" + topic + "]: Received " + processedRecords.length().toString() + " records");
    foreach var processedRecord in processedRecords {
        error|string|() response = ();
        if topic == "course_view" {
            response = viewCourse(check processedRecord.cloneWithType(CourseViewMessage));
        } else if topic === "course_update" {
            response = updateCourse(check processedRecord.cloneWithType(CourseUpdateMessage));
        } else if topic === "course_acknowledge" {
            response = acknowledgeCourse(check processedRecord.cloneWithType(CourseAcknowledgeMessage));
        } else if topic === "course_generate" {
            response = generateCourse(check processedRecord.cloneWithType(CourseGenerateMessage));
        } else if topic === "course_approve" {
            response = approveCourse(check processedRecord.cloneWithType(CourseApproveMessage));
        } else if topic === "lecturer_update" {
            response = lecturerUpdate(check processedRecord.cloneWithType(LecturerUpdateMessage));
        } else if topic === "student_update" {
            response = studentUpdate(check processedRecord.cloneWithType(StudentUpdateMessage));
        }
        BaseMessage msg = check processedRecord.cloneWithType(BaseMessage);
        ApiResponseMessage responseMsg = {
            requestId: msg.requestId,
            'error: (),
            message: ""
        };
        if response is error {
            responseMsg.'error = response.message();
        } else if (response is string) {
            responseMsg.message = response;
        }
        kafka:Producer kafkaProducer = check getKafkaProducer();
        check kafkaProducer->send({
            topic: "api_response",
            value: responseMsg.toJsonString().toBytes()
        });
        check kafkaProducer->close();
        log:printInfo("[topic:api_response]: Sent response");
    }
}

service kafka:Service on new kafka:Listener(bootstrapServer, {
    groupId: "course_view",
    topics: ["course_view"],
    pollingInterval: 1,
    autoCommit: false
}) {
    remote function onConsumerRecord(kafka:Caller caller, kafka:ConsumerRecord[] records) returns error? {
        string topic = "course_view";
        error? ret = processTopic(caller, records, topic);
        if ret is error {
            log:printError("[topic:" + topic + "]: ", ret);
        }
    }
}

service kafka:Service on new kafka:Listener(bootstrapServer, {
    groupId: "course_update",
    topics: ["course_update"],
    pollingInterval: 1,
    autoCommit: false
}) {
    remote function onConsumerRecord(kafka:Caller caller, kafka:ConsumerRecord[] records) returns error? {
        string topic = "course_update";
        error? ret = processTopic(caller, records, topic);
        if ret is error {
            log:printError("[topic:" + topic + "]: ", ret);
        }
    }
}

service kafka:Service on new kafka:Listener(bootstrapServer, {
    groupId: "course_acknowledge",
    topics: ["course_acknowledge"],
    pollingInterval: 1,
    autoCommit: false
}) {
    remote function onConsumerRecord(kafka:Caller caller, kafka:ConsumerRecord[] records) returns error? {
        string topic = "course_acknowledge";
        error? ret = processTopic(caller, records, topic);
        if ret is error {
            log:printError("[topic:" + topic + "]: ", ret);
        }
    }
}

service kafka:Service on new kafka:Listener(bootstrapServer, {
    groupId: "course_generate",
    topics: ["course_generate"],
    pollingInterval: 1,
    autoCommit: false
}) {
    remote function onConsumerRecord(kafka:Caller caller, kafka:ConsumerRecord[] records) returns error? {
        string topic = "course_generate";
        error? ret = processTopic(caller, records, topic);
        if ret is error {
            log:printError("[topic:" + topic + "]: ", ret);
        }
    }
}

service kafka:Service on new kafka:Listener(bootstrapServer, {
    groupId: "course_approve",
    topics: ["course_approve"],
    pollingInterval: 1,
    autoCommit: false
}) {
    remote function onConsumerRecord(kafka:Caller caller, kafka:ConsumerRecord[] records) returns error? {
        string topic = "course_approve";
        error? ret = processTopic(caller, records, topic);
        if ret is error {
            log:printError("[topic:" + topic + "]: ", ret);
        }
    }
}

service kafka:Service on new kafka:Listener(bootstrapServer, {
    groupId: "lecturer_update",
    topics: ["lecturer_update"],
    pollingInterval: 1,
    autoCommit: false
}) {
    remote function onConsumerRecord(kafka:Caller caller, kafka:ConsumerRecord[] records) returns error? {
        string topic = "lecturer_update";
        error? ret = processTopic(caller, records, topic);
        if ret is error {
            log:printError("[topic:" + topic + "]: ", ret);
        }
    }
}

service kafka:Service on new kafka:Listener(bootstrapServer, {
    groupId: "student_update",
    topics: ["student_update"],
    pollingInterval: 1,
    autoCommit: false
}) {
    remote function onConsumerRecord(kafka:Caller caller, kafka:ConsumerRecord[] records) returns error? {
        string topic = "student_update";
        error? ret = processTopic(caller, records, topic);
        if ret is error {
            log:printError("[topic:" + topic + "]: ", ret);
        }
    }
}

public function main() returns error? {
    check initDatabase();
}
