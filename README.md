# NUST Course Outline Manager

## Usage

### Dependencies

- Kafka - 2.13-3.0.0
- Ballerina - Swan Lake Beta 2
- Kubernetes - v1.22.4
- Minikube - v1.24.0

### Initialize Kafka with the required topics

```bash
chmod +x ./init.sh
./init.sh
```

### Start the Kafka cluster

These should be run from wherever your Kafka instance is installed. E.g. `~/Downloads/kafka_2.13-3.0.0/`;

```bash
# Start zookeeper
bin/zookeeper-server-start.bat config/zookeeper.properties

# Start up 3 Kafka service brokers (in different terminal)
bin/kafka-server-start.sh config/server1.properties
bin/kafka-server-start.sh config/server2.properties
bin/kafka-server-start.sh config/server3.properties
```

### Start Microservices (for debugging without Kubernetes)

Some issues were faced getting the Kubernetes pods to communicate with Kafka running on the host, despite using `host.minikube.internal:9093` instead of `localhost:9093` as the Kafka bootstrap server URL. Thus, the code by default is set to use `localhost` and can be run directly as follows as an alternative.

```bash
bal run services/auth
bal run services/api
bal run services/course_manager
```

### Start Microservices (with Kubernetes)

If you wish to run the Ballerina code using Kubernetes instead of directly, first change all references to `localhost:9093` to `host.minikube.internal:9093`. Then, make sure to download, install and run a Kubernetes cluster with Minikube. Finally, use the following commands to deploy the Ballerina microservices.

```bash
# Make sure our built Docker images will be pushed to Minikube's Docker repository
eval $(minikube docker-env)

cd services
bal build --cloud=k8s auth
bal build --cloud=k8s api
bal build --cloud=k8s course_manager
```

Before deploying the services, we will need to change the generated Kubernetes `.yaml` files to use the local Docker repository. For each service, edit their `deployment.yaml` files to contain the following:

```yaml
...
containers:
- image: "course_manager:latest"
  imagePullPolicy: "IfNotPresent" # <-- Make sure this is present
  ...
```

Then deploy the services to Kubernetes using the following commands.

```bash
kubectl apply -f ./services/auth/target/kubernetes/auth
kubectl apply -f ./services/auth/target/kubernetes/api
kubectl apply -f ./services/auth/target/kubernetes/course_manager
```

> You can confirm that all the deployments started successfully by checking Minikube's dashboard (`minikube dashboard`).

You will additionally need to expose the HTTP API deployment so that it can be access from the host;

```bash
# Expose the deployment
> kubectl expose deployment api-deployment --type=NodePort --name=api-svc-local

# Check Minikube URL
> minikube ip
192.168.49.2

# Check the deployment's port
> kubectl get svc

NAME            TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)          AGE
api-svc         ClusterIP   10.99.213.225   <none>        9090/TCP         0s
api-svc-local   NodePort    10.97.216.172   <none>        9090:32382/TCP   0s
kubernetes      ClusterIP   10.96.0.1       <none>        443/TCP          141m
```

From the above we can see that the HTTP API will be available at `192.168.49.2:32382`. E.g. `192.168.49.2:32382/v1/auth/login`

### Query HTTP Client

Look at the `./api.yml` file for instructions on how to interact with the application. In brief, use `/auth/register` or `/auth/login` to acquire a token and then provide that token to other requests like `/course/view`.

E.g.

```bash
> curl --location --request POST 'http://192.168.49.2:32382/v1/auth/register' \
--header 'Content-Type: application/json' \
--data-raw '{
    "username": "yagami",
    "password": "123456789",
    "userType": "student"
}'

=> {
    "id": 4,
    "username": "yagami",
    "userType": "student",
    "token": "eyJhbGciOiJSUzI1NiIsICJ0eXAiOiJKV1QifQ.eyJpc3MiOiJOVVNUIiwgInN1YiI6InlhZ2FtaSIsICJleHAiOjE2Mzc3OTkxOTYsICJuYmYiOjE2Mzc3MTI3OTYsICJpYXQiOjE2Mzc3MTI3OTZ9.PIZLXtSoxWy-oNay8gc_VyZ0Auuv5qNfzVFoOEJa9uYZdNIGUvFl4HErG5peXy_0l424c_3cipUT3vOzlf9HwZcaBW0RtlFPvqmtginan5lXS8TjaAYkhjyHebVnzkZW5De7uiFPfLfBTUG_jUDZT71lbD_nlbfkRqXl1ynIZF8PU7fzUOA2zKcZjqEU64UoSQzsHEMtYAzd_1soXthWCNFfDcsFuls4aleaTCelSIMPVebqxvXdymAczBSWGDuBB_JkACLZrTxcQrdHlm8OUmGp9ZLsLHc6F3NOoilI5oUbQYz2HHNJTnk9CL-uHy6LnseuCxqTt6QgI0Pp2Vf7sg"
}

> curl --location --request POST 'http://192.168.49.2:32382/v1/course/view/?courseIds=DSA621S&token=eyJhbGciOiJSUzI1NiIsICJ0eXAiOiJKV1QifQ.eyJpc3MiOiJOVVNUIiwgInN1YiI6InlhZ2FtaSIsICJleHAiOjE2Mzc3OTkxOTYsICJuYmYiOjE2Mzc3MTI3OTYsICJpYXQiOjE2Mzc3MTI3OTZ9.PIZLXtSoxWy-oNay8gc_VyZ0Auuv5qNfzVFoOEJa9uYZdNIGUvFl4HErG5peXy_0l424c_3cipUT3vOzlf9HwZcaBW0RtlFPvqmtginan5lXS8TjaAYkhjyHebVnzkZW5De7uiFPfLfBTUG_jUDZT71lbD_nlbfkRqXl1ynIZF8PU7fzUOA2zKcZjqEU64UoSQzsHEMtYAzd_1soXthWCNFfDcsFuls4aleaTCelSIMPVebqxvXdymAczBSWGDuBB_JkACLZrTxcQrdHlm8OUmGp9ZLsLHc6F3NOoilI5oUbQYz2HHNJTnk9CL-uHy6LnseuCxqTt6QgI0Pp2Vf7sg'
```

## Communication (Kafka)

### Topics

- course_view
- course_update
- course_acknowledge
- course_generate
- course_approve
- student_update
- lecturer_update
- api_response - Returns arbitrary messages
- auth_register
- auth_register_response
- auth_login
- auth_login_response
- auth_validate
- auth_validate_response

## Micro services

Kafka is used as the message broker between the services.

- API - Ballerina-based HTTP API provider
- Auth - User authentication and management
- Course Manager - Main course management service
