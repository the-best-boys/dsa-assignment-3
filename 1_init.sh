#!/bin/bash

PORT=9093
BOOTSTRAP_SERVER="localhost:$PORT"
KAFKA_DIR="$1" # Kafka installation directory

# Create the required topics
for topic in course_view course_update course_acknowledge course_generate course_approve student_update lecturer_update api_response auth_register auth_register_response auth_login auth_login_response auth_validate auth_validate_response; do
  "$KAFKA_DIR/bin/kafka-topics.sh" --create --replication-factor 3 --partitions 1 --topic $topic --bootstrap-server $BOOTSTRAP_SERVER
done